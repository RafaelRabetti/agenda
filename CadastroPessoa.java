import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa(); 
      
    do {
        System.out.println("\n*** MENU ***\nDigite adicionar para adicionar pessoa.\nDigite remover para remover pessoa.\nDigite pesquisar para pesquisar uma pessoa.\nDigite sair para sair.\n");
        entradaTeclado = leitorEntrada.readLine();

        if (entradaTeclado.equalsIgnoreCase("adicionar")){

            //interagindo com usuário
            System.out.println("Digite o nome da Pessoa:");
            entradaTeclado = leitorEntrada.readLine();
            String umNome = entradaTeclado;

            System.out.println("Digite o telefone da Pessoa:");
            entradaTeclado = leitorEntrada.readLine();
            String umTelefone = entradaTeclado;
   

            //adicionando uma pessoa na lista de pessoas do sistema
            Pessoa umaPessoa = new Pessoa(umNome, umTelefone);
            String mensagem = umControle.adicionar(umaPessoa);
            System.out.println(mensagem);
            }

       else if (entradaTeclado.equalsIgnoreCase("remover")){

            System.out.println("Digite o nome da pessoa que você quer remover:");
            entradaTeclado = leitorEntrada.readLine();
            String umNome = entradaTeclado;
    
            //removendo uma pessoa na lista de pessoas do sistema
	    String mensagem = umControle.remover(umNome);
	    System.out.println(mensagem);
            }


	else if (entradaTeclado.equalsIgnoreCase("pesquisar")){

            System.out.println("Digite o nome da pessoa que você quer pesquisar:");
            entradaTeclado = leitorEntrada.readLine();
            String umNome = entradaTeclado;

            //buscando pessoa na lista de pessoas
	    Pessoa umaPessoa = umControle.pesquisar(umNome);
	    System.out.println(umaPessoa);
            if (umaPessoa != null) {
                System.out.println("\n******** Pessoa encontrada com sucesso ********\n");
            }
            else  System.out.println("\n** Pessoa não encontrada **\n");
 
           
        }
    } 
    while (!entradaTeclado.equalsIgnoreCase("sair"));
    //conferindo saída
    System.out.println("=================================");
    System.out.println("=)");

  }

}

