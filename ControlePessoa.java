import java.util.ArrayList;

public class ControlePessoa{

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
  
        public String adicionar(Pessoa umaPessoa) {
            String mensagem = "\n******** Pessoa adicionada com Sucesso! ********\n";
	    listaPessoas.add(umaPessoa);
	    return mensagem;
        }

    
        public String remover(String umNome) {
	    String mensagem = "\n******** Pessoa removida com Sucesso! ********\n";
            for (Pessoa umaPessoa: listaPessoas) {
                if (umaPessoa.getNome().equalsIgnoreCase(umNome)){
                listaPessoas.remove(umaPessoa);
                return mensagem;
            }
            }
           return null;
        }
    

        public Pessoa pesquisar(String umNome) {
            for (Pessoa umaPessoa: listaPessoas) {
                if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
            }
            return null;
        }    
}
